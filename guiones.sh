#!/bin/bash
# Cambiar espacios y puntos por guiones
# Por Moribundo Insurgente
clear
echo "Cambiando espacios por guiones..."
echo
sleep 2
for FILE in * ; do NUEVOFICHERO=`echo $FILE | sed 's/ /_/g'`; mv "$FILE" $NUEVOFICHERO 2>/dev/null; done
