#!/bin/bash

# Script de ejemplo
# Menu para elegir tareas de sistema

x=0
y=4

while [ $x -le $y ];
do
clear

echo "1. Numero de procesos"
echo "2. Espacio en disco"
echo "3. Usuarios activos"
echo "4. Carga del sistema"
echo "5. Salir"

read x
	case $x in

		1)
			clear
			ps xa | wc -l
			echo "Pulsa una tecla para seguir..."
			read
			;;
		2)
			clear
			df -h
			echo "Pulsa una tecla para seguir..."
			read
			;;
		3)
			clear
			w
			echo "Pulsa una tecla para seguir..."
			read
			;;
		4)
			clear
			uptime
			echo "Pulsa una tecla para seguir..."
			read
			;;
		*)
			clear
			;;
	esac
done


