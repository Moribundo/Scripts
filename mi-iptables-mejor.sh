*filter

#listar
#iptables -L

#ver
#iptables -S

#flushear
#iptables -F

#iptables-restore /etc/network/iptables

# queremos que eso sea permanente

:INPUT ACCEPT [0:0]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
:fail2ban-ssh - [0:0]
-A INPUT -p tcp -m multiport --dports 22 -j fail2ban-ssh
-A fail2ban-ssh -j RETURN

# Se dropea todo por defecto
#-P INPUT DROP
#-P FORWARD DROP
#-P OUTPUT DROP

# Se permiten conexiones entrantes nuevas SSH y se permiten de salida las establecidas
-A INPUT -i eth0 -p tcp --dport 22 -m state --state NEW,ESTABLISHED -j ACCEPT
-A OUTPUT -o eth0 -p tcp --sport 22 -m state --state ESTABLISHED -j ACCEPT

# Se permiten conexiones entrantes HTTP y se permiten de salida las establecidas y las nuevas
-A INPUT -i eth0 -p tcp --dport 80 -m state --state NEW,ESTABLISHED -j ACCEPT
-A OUTPUT -o eth0 -p tcp --sport 80 -m state --state NEW,ESTABLISHED -j ACCEPT

# Se permiten conexiones salientes nuevas SSH y se permiten de entrada las establecidas
-A OUTPUT -o eth0 -p tcp --dport 22 -m state --state NEW,ESTABLISHED -j ACCEPT
-A INPUT -i eth0 -p tcp --sport 22 -m state --state ESTABLISHED -j ACCEPT

# Se permiten conexiones entrantes HTTPS y se permiten de salida las establecidas
-A INPUT -i eth0 -p tcp --dport 443 -m state --state NEW,ESTABLISHED -j ACCEPT
-A OUTPUT -o eth0 -p tcp --sport 443 -m state --state NEW,ESTABLISHED -j ACCEPT

# Se permite acceso al PROXY HTTP
-A OUTPUT -o eth0 -p tcp --dport 8080 -j ACCEPT

# Se permiten conexiones para MYSQL
-A INPUT -i eth0 -p tcp --dport 3306 -m state --state NEW,ESTABLISHED -j ACCEPT
-A OUTPUT -o eth0 -p tcp --sport 3306 -m state --state NEW,ESTABLISHED -j ACCEPT

# Se permiten conexiones para resolución de nombres tcp y udp
-A INPUT -i eth0 -p tcp --dport 53 -m state --state NEW,ESTABLISHED -j ACCEPT
-A INPUT -i eth0 -p udp --dport 53 -m state --state NEW,ESTABLISHED -j ACCEPT
-A OUTPUT -o eth0 -p tcp --sport 53 -m state --state NEW,ESTABLISHED -j ACCEPT
-A OUTPUT -o eth0 -p udp --sport 53 -m state --state NEW,ESTABLISHED -j ACCEPT
COMMIT


#Activando ip-forwarding
echo 1 > /proc/sys/net/ipv4/ip_forward

#proteccion SYN_FLOODING
iptables -N SYNFLOOD
iptables -A INPUT -i eth0 -p tcp --syn -j SYNFLOOD
iptables -A SYNFLOOD -m limit --limit 1/s --limit-burst 10 -j RETURN
iptables -A SYNFLOOD -j DROP


