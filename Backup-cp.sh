#!/bin/bash
clear
echo " ###################################"
echo "##            	                    ##"
echo "##    SCRIPT COPIAS DE SEGURIDAD    ##"
echo "##                                  ##"
echo "##          Por ZX80                ##"
echo "##                                  ##"
echo " ####################################"
echo
echo "Pulse enter para empezar a copiar."
read -n 0 -ers
echo
echo
echo
sleep 3s
cp -Rpv CGT /home/zx80/Backup/
cp -Rpv Basura /home/zx80/Backup/
cp -Rpv Carteles /home/zx80/Backup/
cp -Rpv docs /home/zx80/Backup/
cp -Rpv sites /home/zx80/Backup/
# cp -Rpv Temp /home/zx80/Backup/
cp -Rpv z /home/zx80/Backup/
# cp -Rpv .aMule /home/zx80/Backup/
cp -Rpv .config /home/zx80/Backup/
cp -Rpv .fonts /home/zx80/Backup/
cp -Rpv .gconf /home/zx80/Backup/
cp -Rpv .gnome2 /home/zx80/Backup/
cp -Rpv .gnupg /home/zx80/Backup/
cp -Rpv .gqview /home/zx80/Backup/
cp -Rpv .gwhere /home/zx80/Backup/
cp -Rpv .hplip /home/zx80/Backup/
cp -Rpv .liferea_1.8 /home/zx80/Backup/
cp -Rpv .mozilla /home/zx80/Backup/
cp -Rpv .mplayer /home/zx80/Backup/
cp -Rpv .sane /home/zx80/Backup/
#cp -Rpv .thunderbird /home/zx80/Backup/
cp -Rpv .xmame /home/zx80/Backup/


sleep 4s
echo .
echo .
echo .
echo .
echo "Copia realizada correctamente."
echo "Pulsa Enter para tarear y copiar a Store."
read -n 0 -ers
tar -cf Backup.tar Backup
cp Backup.tar /mnt/store/
echo .
echo .
echo "Purgando directorio e iniciando copia...."
rm -rf Backup
mkdir Backup
echo "    Listo !!!!"

