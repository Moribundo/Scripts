#!/bin/bash

clear
echo
IP=$(curl -s https://www.icanhazip.com)
tput setaf 2; tput bold
echo "Tu IP: " $IP
echo
tput setaf 9; tput bold
echo "La temperatura: "
tput sgr0
echo
sensors | sed '1,3d' | cut -c 1-24
tput setaf 4; tput bold
echo "Capacidad actual en /: "
tput sgr0
echo
df -h / | cut -c 16-60
echo
tput setaf 4; tput bold
echo "Capacidad actual en /home: "
tput sgr0
echo
df -h /home/zx80 | cut -c 16-60
echo