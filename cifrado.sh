#!/bin/bash
# Script de cifrado/descifrado con GnuPG

x=0
y=2

while [ $x -le $y ];
do

clear
echo "1- Cifrar"
echo "2- Descifrar"
echo "3- Salir"
read x

case $x in

   1) 
     clear
     echo -n "Nombre del archivo a cifrar: "; read CIF
     echo "Cifrando ..."
sleep 2
	gpg -er "Moribundo Insurgente" $CIF
   ;;

   2)
     clear
     echo -n "Nombre del archivo a descifrar: "; read DESCIF
     echo -n "Nombre del archivo una vez descifrado: "; read DESCIF2
     echo "Descifrando ..."
sleep 2
gpg -o $DESCIF2 -d $DESCIF
   ;;

   *)
     clear
     echo
     echo
     exit
   ;;
esac
done
