#! /bin/sh

#insertar m�dulos
modprobe ip_tables
modprobe ip_conntrack
modprobe ip_conntrack_ftp
modprobe ip_nat_ftp

#reglas
iptables -F

#logs
#iptables -A INPUT -p tcp --dport 1:1024 -j LOG --log-prefix "INTENTOS: "
#iptables -A INPUT -p icmp --icmp-type echo-request -j LOG --log-prefix "PINGS: "

#proteccion contra el protocolo ICMP
iptables -A INPUT -p icmp --icmp-type echo-request -j DROP

#aceptando conexiones de privoxy
iptables -A INPUT -p tcp -s 127.0.0.1 --dport 8118 -j ACCEPT  
iptables -A INPUT -p udp -s 127.0.0.1 --dport 8118 -j ACCEPT 

#proteccion contra el protocolo UDP
iptables -A INPUT -p udp -s 0.0.0.0 -j DROP

#seteando la politica de INPUT en DROP
#iptables -P INPUT DROP
#iptables -P OUTPUT ACCEPT

#permitiendo el paso a nuestra direccion ip
iptables -A INPUT -s 127.0.0.1 -j ACCEPT

#permitir todo el trafico interno
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

#aceptando las respuestas de los servidores
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

#cerrando rango de puertos privilegiados
iptables -A INPUT -s 0.0.0.0/0  -p tcp --dport 22:1024 -j DROP
iptables -A INPUT -s 0.0.0.0/0  -p udp --dport 22:1024 -j DROP


#cerrando puertos aleatorios
iptables -A INPUT -p tcp --dport 3306 -j DROP
iptables -A INPUT -p udp --dport 3306 -j DROP
iptables -A INPUT -p tcp --dport 6000 -j DROP
iptables -A INPUT -p udp --dport 6000 -j DROP
iptables -A INPUT -p tcp --dport 10000 -j DROP
iptables -A INPUT -p udp --dport 10000 -j DROP
iptables -A INPUT -p tcp --dport 8118 -j DROP
iptables -A INPUT -p udp --dport 8118 -j DROP
iptables -A INPUT -p tcp --dport 1702 -j DROP
iptables -A INPUT -p udp --dport 1702 -j DROP
iptables -A INPUT -p tcp --dport 1757 -j DROP
iptables -A INPUT -p udp --dport 1757 -j DROP
iptables -A INPUT -p tcp --dport 1277 -j DROP
iptables -A INPUT -p udp --dport 1277 -j DROP
iptables -A INPUT -p tcp --dport 1419 -j DROP
iptables -A INPUT -p udp --dport 1419 -j DROP
iptables -A INPUT -p tcp --dport 1363 -j DROP
iptables -A INPUT -p udp --dport 1363 -j DROP
iptables -A INPUT -p tcp --dport 1219 -j DROP
iptables -A INPUT -p udp --dport 1219 -j DROP
iptables -A INPUT -p tcp --dport 3306 -j DROP
iptables -A INPUT -p udp --dport 3306 -j DROP
iptables -A INPUT -p tcp --dport 1675 -j DROP
iptables -A INPUT -p udp --dport 1675 -j DROP


#cerrando conexiones al Xwindows
iptables -A INPUT -p tcp --dport 6001:6065 -j DROP
iptables -A INPUT -p udp --dport 6001:6065 -j DROP

#Abriendo ftp
iptables -A INPUT -p tcp --dport 20:21 -j ACCEPT

#Activando ip-forwarding
echo 1 > /proc/sys/net/ipv4/ip_forward

#proteccion SYN_FLOODING
iptables -N SYNFLOOD
iptables -A INPUT -i eth0 -p tcp --syn -j SYNFLOOD
iptables -A SYNFLOOD -m limit --limit 1/s --limit-burst 10 -j RETURN
iptables -A SYNFLOOD -j DROP


