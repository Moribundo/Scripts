#!/bin/bash

djvudigital --dpi=500 $1 $1.djvu
for FILE in *.pdf.djvu; do NEWFILE=`echo "$FILE" | sed 's/.pdf.djvu$/.djvu/'` ; mv "$FILE" $NEWFILE ; done
