#!/bin/bash
# Uso de las secure-tools para limpiar la memoria
# Limpia swap, ram y directorio /var

	# smem
	# sswap
	# srm -r /var


clear
echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="
echo "  VIGILA TU PRIVACIDAD. MUERTE A LOS DATOS"
echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="
echo
echo "Pulsa enter para empezar"
read -n 0 -ers
echo
echo
echo
echo "Eliminando swap..."
sswap
sleep 2s
echo "Eliminando RAM..."
smem
sleep 2s
echo "Eliminando /var..."
srm -r /var
sleep 2s
#echo "Apagando..."
#shutdown -h now
