#!/bin/bash

x=0
y=3

while [ $x -le $y ];
do

clear
echo "ELIJE QUE SEPULTURA HACES HOY"
echo "1- Cavar una tumba."
echo "2- Abrir una tumba."
echo "3- Cerrar una tumba."
echo "4- Salir"

read x

case $x in

   1) 
     clear
     echo -n "Dale un nombre a la tumba: "; read NOM
     echo -n "Dale espacio en MB: "; read MB
     echo -n "Nombre de la llave: "; read KEY
     echo "CAVANDO TUMBA"
     tomb dig -s $MB $NOM
     echo
     echo "Creando llave..."
     tomb forge $KEY.key
     echo
     echo "Bloqueando tumba..."
     tomb lock $NOM -k $KEY.key
     echo "Pulsa una tecla para continuar...."
     read
   ;;

   2)
     clear
     echo "ABRIR TUMBA"
     echo -n "Nombre de la tumba: "; read NOM
     echo -n "Nombre de la llave: "; read KEY
     echo
     tomb open $NOM -k $KEY.key
     echo "Pulsa una tecla para continuar...."
     read
   ;;

   3)
     clear
     echo "CERRAR TUMBA"
     echo -n "Dale un nombre a la tumba: "; read NOM
     tomb close $NOM
     echo "Pulsa una tecla para continuar...."
     read 
   ;;

   *)
     clear
     echo "AMEN..."
     echo
     exit
   ;;
esac
done
