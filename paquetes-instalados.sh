#!/bin/bash

# Listar los paquetes instalados mediante pacman

pacman -Qei | awk '/^Nombre/ { name=$3 } /^Grupos/ { if ( $3 != "base" && $3 != "base-devel" ) { print name } }' > LISTADEPAQUETES

