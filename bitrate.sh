#!/bin/bash
#mb * 8388.608 / seg - audio -15
#800 * 8388.608 / 6760 -40 -15

clear
echo "CALCULAR LOS SEGUNDOS"
echo
echo
read -p "Introduce las horas del video: " hor
read -p "Introduce los minutos del video: " min
read -p "Introduce los segundos del video: " seg

HORA=$(( $hor*3600 ))
MIN=$(( $min*60 )) 
CONV=$(( $HORA + $MIN + $seg ))
echo
echo "Segundos del video: " $CONV

# Cambiamos el color de las letras, y en base a que 80 minutos son 700 Mb en calidad CD, hacemos el cálculo y dejamos las letras del color normal

tput setaf 3
echo "Los Mb del video deberían ser: " $(( $CONV * 700 / 4800 ))
tput sgr0
echo
echo
echo
echo "CALCULAR EL BITRATE"
echo
read -p "Introduce los Megas del video resultante: " MB

# Se calcula el bitrate final teniendo en cuenta los valores del inicio del script, aplicando el factor de corrección al audio, que lo he dejado fijo a 128 bits. Descomentar la siguiente linea si se quieren poner otros valores

#read -p "Introduce bitrate del audio: " AUD
AUD=128
SUMAUDIO=$(( $AUD+15 ))
MEGAS=$(( $MB*8389/($CONV)-$SUMAUDIO ))

echo
tput setaf 5
echo "El bitrate para el video es:" $MEGAS
tput sgr0
echo
echo
