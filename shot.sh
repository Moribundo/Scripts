#!/bin/bash
# ===================================================== 
# This script takes screenshots of a movie
# Depends on mplayer and imagemagick
#
# Made by Starlite
# http://starl1te.wordpress.com/

# Feel free to share and modify, but
# Please, let me know if you made improvements.
# Пожалуйста, дайте мне знать, если вы улучшили этот скрипт.
# =====================================================

usage="Type shot -h for help"
_help(){
echo -e "\nusage: shot [options] [file]\n
 Options:
  -t <time> - set time (in minutes) between screenshots; the number of screenshots is calculated automatically.
  -n <number> - set a fixed number of screenshots to take.
  -r <percent> - change the size of the output image. Less than 40% is recommended.
  -h - display this help message\n
Only one option at a time is possible.
If you don't like taken screenshots, try to run the script once more
This script depends on Mplayer and ImageMagic.\n
Usage example:
shot -n 25 -r 35% ~/films/film.avi\n"
}

shot(){
# Making screenshots...
for i in `seq 1 $shots_number`;
do
  randomiser=$RANDOM; let "randomiser %= 25"
  hop=`echo $[$shot_time*60*$i+$randomiser]`
  mplayer -ss $hop -noautosub -frames 1 -ao null -vo png "$file_path" > /dev/null 2>&1
  mv 00000001.png /tmp/shots/$i.png
  echo -ne "Taking screenshot #${i} \r"
done
  echo "Taking screenshots...           [OK]"
}


# ====== first step is here! ;-) ========
# Checking options...
while getopts ":t:n:r:h" option
	do
		case $option in
		t ) shot_time=$OPTARG; opt=_time;;
		n ) shots_number=$OPTARG; opt=_num;;
		h ) _help; opt=1; exit 1;;
		r ) res=$OPTARG;;
		: ) echo "No argument given"; opt=1; exit 1;;
		* ) echo "Unknown option"; echo $usage; opt=1; exit 1;;
		esac
	done

if [ "$res" == "" ]; then res=40%; fi
if [ "$opt" == "" ]; then echo "No option given!"; echo $usage; exit 1; fi
shift $(($OPTIND - 1))
if [ "$1" == "" ]; then echo "No file given!"; echo $usage; exit 1; fi
mkdir /tmp/shots

# Parsing files...
while [ "$1" != "" ]
do
  file_path=$1
  file_name_ext=${file_path##*/}
  file_name=`echo "$file_name_ext" | sed '$s/....$//'`
  randomiser=0
  movdir=`dirname "$file_path"`
	if [ "$movdir" == "." ]; then
	movdir=`pwd`
	file_path=$movdir/$file_path
	fi
  cd "$movdir"
echo "Processing file $file_name..."

# Getting movie length...
length=`mplayer -identify "$file_path" -frames 1 -ao null -vo null 2>/dev/null \
| grep LENGTH | sed -e 's/^.*=//' -e 's/[.].*//'`

if [ "$length" == "" ]; then echo "Error! Can't get the length of the movie."; exit 1; fi

if [ "$opt" == "_time" ]; then
	shots_number=`echo $[$length/60/$shot_time]`
	shot
elif [ "$opt" == "_num" ]; then
	shot_time=`echo $[$length/$shots_number/60]`
	shot
fi

# Placing all taken screenshots in one picture...
echo -n "Putting screenshots together..."
cd /tmp/shots/
montage -geometry +2+2 -compress jpeg `ls *.png | sort -n` "$file_name".jpg
mogrify -resize $res "$file_name".jpg
echo " [OK]"
echo -n "Getting video info..."
size=`stat -c%s "$file_path"`
size=`echo $[$size/1024/1024]`
format=`mplayer -frames 1 -ao null -vo null -identify 2>/dev/null "$file_path" | grep VIDEO: | cut -d " " -f 5`
length=`echo $[$length/60]`
# It's a tricky code here, it adds some info about the movie to the output image.
echo -e "File name: $file_name_ext\nSize: $size Mb\nResolution: $format\nDuration: $length min." | convert -pointsize 16 -trim +repage text:- text.jpg
convert "$file_name".jpg -splice 0x70 -draw 'image over 5,5 0,0 text.jpg' "$movdir/$file_name".jpg
echo "           [OK]"; echo
cd "$movdir"
shift
done

rm -r /tmp/shots
echo "Done"
