#!/bin/bash

# Giteando que es gerundio
# Por Moribundo Insurgente

x=0
y=2

while [ $x -le $y ];
do
clear

echo "1. Clonar repo"
echo "2. Gitear cambios"
echo "3. Salir"

read x
	case $x in

		1)
			clear
			read -p "Introduce la rama: " rama
			git clone https://git.disroot.org/Moribundo/$rama
			echo "Pulsa una tecla para menu"
			read
			;;
		2)
			clear
			echo "Giteando cambios..."
			echo
			sleep 1
			read -p "Introduce la rama: " rama			
			cd $rama			
			git add . 
			read -p "Introduce commit: " commit
			git commit -m "$commit" -a
			sleep 1
			echo
			echo
			read -p "Introduce ruta a la rama https://git.disroot.org/Moribundo/ (ejemplo Scripts/Media): " ruta
			git push https://git.disroot.org/Moribundo/$ruta
			echo
			echo
			echo "Push terminado con éxito. Pulsa una tecla para menu"
			read
			;;
		*)
			clear
			;;
	esac
done
