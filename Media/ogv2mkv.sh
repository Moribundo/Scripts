#!/bin/bash
# Conversor de archivos AVI a MKV
# Por Moribundo Insurgente

clear
echo CONVERSOR DE AVI A MKV
echo
echo

# Cambiar espacios por guion bajo

   for FILE in *.ogv ; do NEW=`echo $FILE | sed 's/ /_/g'`; mv "$FILE" $NEW; done

# Convertir mediante HandBrakeCLI

for file in *.ogv; do HandBrakeCLI --preset-import-file Handbrake_h264.json -i $file -o "$file.mkv"; done; rm *.ogv

# Eliminar la extensión duplicada .avi.mkv
echo
echo
echo Eliminando extension duplicada...

   for FILE in *.ogv.mkv ; do NEWFILE=`echo "$FILE" | sed 's/.ogv.mkv$/.mkv/'` ; mv "$FILE" $NEWFILE ; done

