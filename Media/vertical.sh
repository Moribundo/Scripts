#!/bin/bash
# Ajustar un video vertical a horizontal
# Por Moribundo Insurgente

clear
echo AJUSTE VERTICALIDAD
echo
echo

# Convertir mediante HandBrakeCLI

for file in *.mp4; do HandBrakeCLI --preset-import-file Vertical.json -i $file -o "$file.mkv"; done; rm *.mp4

# Eliminar la extensión duplicada .mp4.mkv
echo
echo
echo Eliminando extension duplicada...

   for FILE in *.mp4.mkv ; do NEWFILE=`echo "$FILE" | sed 's/.mp4.mkv$/.mkv/'` ; mv "$FILE" $NEWFILE ; done

