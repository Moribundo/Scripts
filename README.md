# SCRIPTS

Los scripts que están en este repo son muy básicos, no domino bash pero hago mis pequeñas automatizaciones.




## **Listado de scripts**

Todos son propios a excepción del script **spectre_meltdown_checker.sh** de Stephane Lesimple para detectar la vulnerabilidad Spectre y Meltdown, y **shot.sh**, un capturador de pantalla que pertenece a Starlite.

1. **acentos**: Elimina acentos en los nombres de archivo.
2. **backup-cp**:  Una automatización simple para mis back-ups.
3. **backup-rsync**: Script de back-up mediante rsync.
4. **bitrate**: Calculadora de bitrate para recodificar videos.
5. **cambio IP**:, Cambia la IP del ruter, reiniciandolo
6. **cifrado**: Cifra y descifra con GPG.
7. **date_sys_mod**: Menú para cambiar la fecha del sistema y actualizarla.
8. **Ejemplo01_Menu**: n ejemplo de menú de selección.
9. **freeram**: Libera memoria.
10. **guiones**: Cambia espacios por guiones en los nombres de archivo.
11. **IPlocation**: Devuelve tu localización IP.
12. **Luks**: Menú de cifrado con LUKS.
13. **make-iso**: Alias de creación de una ISO.
14. **mi-iptables**: Mis reglas de filtrado en iptables.
15. **mi-iptables-mejor**: Versión mejorada del anterior.
16. **paquetes-instalados**: Comando pacman para listar los paquetes instalado en el sistema.
17. **djvu**: Conversor de pdf a djvu un archivo dado.
18. **djvu-masivo**: Conversor de pdf a djvu todos los pdf.
19. **sepultura**: Menú de cifrado con tomb.
20. **shot**: Capturaor de pantalla.
21. **spectre-meltdown-checker**: Script para detectar si nuestro equipo es vulnerable a spectre y meltdown
22. **wipemem**: Borrar memoria, swap, /var y luego apaga la máquina


***
## **Listado de scripts en Media**
Estos scripts los hice para convertir formatos de video y audio.

1. **avi2mkv.sh**: Convertir avi a mkv bajo codec h264.
2. **Handbrake_h264.json**: Archivo CLI de Handbrake para usar con los scripts.
3. **Handbrake_h265.json**: Archivo CLI de Handbrake para usar con los scripts (cambiar h264 por h265 en los scripts).
4. **mp32ogg**: Conversor de mp3 a ogg (necesarios mpg321 y oggenc).
5. **mp42mkv**: Convertir mp4 a mkv bajo codec h264.
6. **ogv2mkv**: Convertir ogv a mkv bajo codec h264.
7. **Vertical.json**: Archivo CLI de Handbrake bajo codec h264 para usar con el siguiente script.
8. **vertical**: Convertir videos de movil verticales a horizontales.


***
## **Listado de scripts en XFE**
Estos scripts pertenecen al gestor de archivos XFE que es el que utilizo y el cual tiene soporte para scipts.


1. **aes-c**: Cifrar con el programa aescrypt.
2. **cifrar**: Para cifrar archivos con gpg.
3. **descifrar**: Descifra archivos con gpg.
4. **shred**: Elimina datos mediante 10 pasdas.
5. **srm**: Versión moderna de shred contenido en el paquete "secure delete" que borra archivos mediante 30 pasdas.


***

`Moribundo Insurgente`
<br>
<br>
