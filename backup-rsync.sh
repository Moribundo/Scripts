#!/bin/bash
clear
tput setaf 2
echo " ###################################"
echo "##            	                    ##"
echo "##    SCRIPT COPIAS DE SEGURIDAD    ##"
echo "##            RSYNC                 ##"
echo "##          Por ZX80                ##"
echo "##                                  ##"
echo " ####################################"
echo
tput sgr0
tput setaf 3
sleep 3s
echo .
echo .
echo .
echo .
echo "Copiando ..."
tput sgr0
# Para excluir archivos
# rsync -avh .aMule --exclude 'Incoming' --exclude 'Temp' /home/zx80/backup/

# Para excluir de una lista con archivos y carpetas
# rsync -avz --exclude-from 'lista_excluidos.txt' source/ destino/

echo "Copiando 0fotos"
sleep 2s
rsync -avh 0fotos /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando Mail"
sleep 2s
rsync -avh Mail /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando basura"
sleep 2s
rsync -avh basura /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando descargas"
sleep 2s
rsync -avh descargas --exclude 'quake2' /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando dosbox"
sleep 2s
rsync -avh dosbox --exclude 'quake2' /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando dwhelper"
sleep 2s
rsync -avh dwhelper /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando gPodder"
sleep 2s
rsync -avh gPodder /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando kk"
sleep 2s
rsync -avh kk /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando mierda"
sleep 2s
rsync -avh mierda --exclude 'store/amule' /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando notas"
sleep 2s
rsync -avh notas /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando scribus"
rsync -avh scribus /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando scummvm"
sleep 2s
rsync -avh scummvm /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando sys"
sleep 2s
rsync -avh sys /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando tmp"
sleep 2s
rsync -avh tmp /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando zim-books"
sleep 2s
rsync -avh zim-books /home/zx80/backup/ 1>>salida.txt
echo
echo

tput setaf 2
echo ">>> COPIANDO DIRECTORIOS OCULTOS <<<"
tput sgr0

echo "Copiando HashCheck"
sleep 2s
rsync -avh .HashCheck /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando SoulseekQt"
sleep 2s
rsync -avh .SoulseekQt /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando aMule"
sleep 2s
rsync -avh .aMule --exclude 'Incoming' --exclude 'Temp' /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando audacity-data"
sleep 2s
rsync -avh .audacity-data /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando avidemux"
sleep 2s
rsync -avh .avidemux6 /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando claws-mail"
sleep 2s
rsync -avh .claws-mail /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando config"
sleep 2s
rsync -avh .config /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando cups"
sleep 2s
rsync -avh .cups /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando dosbox"
sleep 2s
rsync -avh .dosbox /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando gimp"
sleep 2s
rsync -avh .gimp-2.8 /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando gnupg"
sleep 2s
rsync -avh .gnupg /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando hplip"
sleep 2s
rsync -avh .hplip /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando icons"
sleep 2s
rsync -avh .icons /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando librewolf"
sleep 2s
rsync -avh .librewolf /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando local"
sleep 2s
rsync -avh .local /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando mame"
sleep 2s
rsync -avh .mame --exclude 'roms' /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando password-store"
sleep 2s
rsync -avh .password-store /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando purple"
sleep 2s
rsync -avh .purple /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando scripts"
sleep 2s
rsync -avh .scripts /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando ssh"
sleep 2s
rsync -avh .ssh /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando ssr"
sleep 2s
rsync -avh .ssr /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando themes"
sleep 2s
rsync -avh .themes /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando tor"
sleep 2s
rsync -avh .tor /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando vvv"
sleep 2s
rsync -avh .vvv /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando xmame"
sleep 2s
rsync -avh .xmame /home/zx80/backup/ 1>>salida.txt
echo

tput setaf 2
echo ">>> COPIANDO ARCHIVOS OCULTOS <<<"
tput sgr0

echo "Copiando bashrc"
sleep 2s
rsync -avh .bashrc /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando bash_profile"
sleep 2s
rsync -avh .bash_profile /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando VVV"
sleep 2s
rsync -avh .VVV /home/zx80/backup/ 1>>salida.txt
echo

echo "Copiando xinitrc"
sleep 2s
rsync -avh .xinitrc /home/zx80/backup/ 1>>salida.txt
echo

tput setaf 2
echo ">>> COPIANDO OTROS ARCHIVOS <<<"
tput sgr0

echo "Pelis buenas"
sleep 2s
rsync -avh Pelis_buenas.odb /home/zx80/backup/ 1>>salida.txt
echo

echo "Pelis buenas pruebas"
sleep 2s
rsync -avh Pelis_buenas-pruebas.odb /home/zx80/backup/ 1>>salida.txt
echo

echo "contratacion"
sleep 2s
rsync -avh contratacion-bckup.odb /home/zx80/backup/ 1>>salida.txt
echo

echo "urls"
sleep 2s
rsync -avh urls /home/zx80/backup/ 1>>salida.txt
echo


tput setaf 2
sleep 3s
echo .
echo .
echo .
echo .
echo "Copia realizada correctamente."
echo
echo "Tareando backup..."
echo .
echo .
# tar -cvzf backup-$(date +%d-%m-%Y).tgz backup 1>>salida_b.txt
tar -cvaf backup-$(date +%d-%m-%Y).tar.zst backup 1>>salida_b.txt

# echo "Purgando directorio e iniciando copia...."
echo .
echo .
echo .
# rm -rf backup
echo "    Listo !!!!"
echo
echo
tput sgr0

# Si ponemos Tmp/ solo copiará los archivos de Tmp, no la carpeta entera