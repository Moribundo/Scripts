#!/bin/sh
clear
echo " ###################################"
echo "##            	                    ##"
echo "##    MUESTRA TU IP Y SU INFO       ##"
echo "##                                  ##"
echo "##          Por ZX80                ##"
echo "##                                  ##"
echo " ####################################"
echo
sleep 1s
echo
# curl -s --connect-timeout 2 ifconfig.co
IP=$(curl -s --connect-timeout 2 icanhazip.com)
echo Tu IP es: $IP
echo
echo Mostrando info:
echo
sleep 1s
curl ipinfo.io/$IP
echo
echo
echo "    Listo !!!!"
echo
echo

