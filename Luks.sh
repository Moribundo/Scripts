#!/bin/bash

x=0
y=3

while [ $x -le $y ];
do

clear
echo "ELIJE QUE HACEMOS HOY"
echo "1- Crear volumen."
echo "2- Montar volumen."
echo "3- Desmontar volumen."
echo "4- Salir"

read x

case $x in

   1) 
     clear
     echo -n "Nombre del volumen a crear: "; read VOL
     echo -n "Dale espacio en MB: "; read MB
     echo -n "Especifica un nombre para /dev/mapper: "; read MAP
     echo -n "Nombre del volumen montado: "; read NOM
     echo 
     echo "CREANDO VOLUMEN"
       dd if=/dev/zero bs=1M count=$MB of=$VOL
     echo
     echo "Formateando volumen..."
       cryptsetup -c aes-xts-plain -s 512 luksFormat $VOL
     echo
     echo "Abriendo volumen..."
       cryptsetup luksOpen $VOL $MAP
     echo "Formateando volumen..."
       mkfs.ext4 /dev/mapper/$MAP -L $NOM -m 2
       cryptsetup luksClose $MAP
     echo "Pulsa una tecla para continuar...."
     read
   ;;

   2)
     clear
     echo "MONTAR VOLUMEN"
     echo -n "Nombre del volumen: "; read VOL
     echo -n "Nombre del mapper: "; read MAP
     echo
       cryptsetup luksOpen $VOL $MAP
     echo "Pulsa una tecla para continuar...."
     read
   ;;

   3)
     clear
     echo "DESMONTAR VOLUMEN"
     echo -n "Nombre del mapper: "; read MAP
       cryptsetup luksClose $MAP
     echo "Pulsa una tecla para continuar...."
     read 
   ;;

   *)
     clear
     echo "HASTA OTRA..."
     echo
     exit
   ;;
esac
done
