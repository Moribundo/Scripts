#!/bin/bash
# Quitar acentos
# Por Moribundo Insurgente

clear
echo "Renombrando archivos..."
echo
sleep 2
rename á a *
rename é e *
rename í i *
rename ó o *
rename ú u *
rename Á A *
rename É E *
rename Í I *
rename Ó O *
rename Ú U *
exit
